#include <QApplication>
#include <QTest>

#include "mainwindowtest.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    MainWindowTest test;

    return QTest::qExec(&test, argc, argv);
}
