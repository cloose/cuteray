#include <QtTest/QtTest>

#include "typeconversions.h"

class TypeConversionsTest : public QObject
{
    Q_OBJECT

private slots:
    void convertsQColorToRgbColor();
    void convertsRgbColorToQColor();
    void convertsQVector3DToPoint3D();
    void convertsPoint3DToQVector3D();
    void convertsQVector3DToVector3D();
    void convertsVector3DToQVector3D();
};

double rounded(double value) {
    return qRound(value * USHRT_MAX) / double(USHRT_MAX);
}

void TypeConversionsTest::convertsQColorToRgbColor()
{
    QColor expected = QColor::fromRgbF(0.8, 0.55, 0.3);

    RgbColor actual = toRgbColor(expected);

    QCOMPARE(actual.red(), expected.redF());
    QCOMPARE(actual.green(), expected.greenF());
    QCOMPARE(actual.blue(), expected.blueF());
}

void TypeConversionsTest::convertsRgbColorToQColor()
{
    RgbColor expected(0.8, 0.55, 0.3);

    QColor actual = toQColor(expected);

    QCOMPARE(actual.redF(), rounded(expected.red()));
    QCOMPARE(actual.greenF(), rounded(expected.green()));
    QCOMPARE(actual.blueF(), rounded(expected.blue()));
}

void TypeConversionsTest::convertsQVector3DToPoint3D()
{
    QVector3D expected(0.875, -0.987654321, 65.0);

    Point3D actual = toPoint3D(expected);

    QCOMPARE(actual.x, expected.x());
    QCOMPARE(actual.y, expected.y());
    QCOMPARE(actual.z, expected.z());
}

void TypeConversionsTest::convertsPoint3DToQVector3D()
{
    Point3D expected(0.875, -0.987654321, 65.0);

    QVector3D actual = toQVector3D(expected);

    QCOMPARE(actual.x(), (float)expected.x);
    QCOMPARE(actual.y(), (float)expected.y);
    QCOMPARE(actual.z(), (float)expected.z);
}

void TypeConversionsTest::convertsQVector3DToVector3D()
{
    QVector3D expected(0.875, -0.987654321, 65.0);

    Vector3D actual = toVector3D(expected);

    QCOMPARE(actual.x, expected.x());
    QCOMPARE(actual.y, expected.y());
    QCOMPARE(actual.z, expected.z());
}

void TypeConversionsTest::convertsVector3DToQVector3D()
{
    Vector3D expected(0.875, -0.987654321, 65.0);

    QVector3D actual = toQVector3D(expected);

    QCOMPARE(actual.x(), (float)expected.x);
    QCOMPARE(actual.y(), (float)expected.y);
    QCOMPARE(actual.z(), (float)expected.z);
}

QTEST_MAIN(TypeConversionsTest)
#include "typeconversionstest.moc"
