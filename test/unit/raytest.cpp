#include <QtTest/QtTest>

#include "ray.h"

class RayTest : public QObject
{
    Q_OBJECT

private slots:
    void returnsPointOnRayAtPassedParameter();
};

void RayTest::returnsPointOnRayAtPassedParameter()
{
    Ray ray {{0.0, 0.0, 0.0}, {1.0, 1.0, 1.0}};

    QCOMPARE(ray.point_at_parameter(0.0), (Point3D {0.0, 0.0, 0.0}));
    QCOMPARE(ray.point_at_parameter(5.5), (Point3D {5.5, 5.5, 5.5}));
    QCOMPARE(ray.point_at_parameter(-1.25), (Point3D {-1.25, -1.25, -1.25}));

    Ray otherRay {{0.5, 1.0, -1.0}, {0.5, 1.0, 100.0}};

    QCOMPARE(otherRay.point_at_parameter(0.0), (Point3D {0.5, 1.0, -1.0}));
    QCOMPARE(otherRay.point_at_parameter(2.0), (Point3D {1.5, 3.0, 199.0}));
}

namespace QTest {

template<>
char *toString(const Point3D &point)
{
    QByteArray ba = "Point3D(";
    ba += QByteArray::number(point.x) + ", " + QByteArray::number(point.y) + ", " + QByteArray::number(point.z);
    ba += ')';
    return qstrdup(ba.data());
}

}

QTEST_MAIN(RayTest)
#include "raytest.moc"
