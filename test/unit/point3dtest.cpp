#include <QtTest/QtTest>

#include "point3d.h"

class Point3DTest : public QObject
{
    Q_OBJECT

private slots:
    void calculatesSumOfPointAndVector();
    void calculatesDifferenceOfTwoPoints();
};

void Point3DTest::calculatesSumOfPointAndVector()
{
    Point3D p {-0.5, 3.25, 0.0};
    Vector3D v {2.0, 1.25, -1.5};
    Point3D expected {1.5, 4.5, -1.5};

    QCOMPARE(p + v, expected);
}

void Point3DTest::calculatesDifferenceOfTwoPoints()
{
    Point3D p1 {-0.5, 3.25, 0.0};
    Point3D p2 {2.0, 1.25, -1.5};
    Vector3D expected {-2.5, 2.0, 1.5};

    QCOMPARE(p1 - p2, expected);
}

namespace QTest {

template<>
char *toString(const Vector3D &vector)
{
    QByteArray ba = "Vector3D(";
    ba += QByteArray::number(vector.x) + ", " + QByteArray::number(vector.y) + ", " + QByteArray::number(vector.z);
    ba += ')';
    return qstrdup(ba.data());
}

template<>
char *toString(const Point3D &point)
{
    QByteArray ba = "Point3D(";
    ba += QByteArray::number(point.x) + ", " + QByteArray::number(point.y) + ", " + QByteArray::number(point.z);
    ba += ')';
    return qstrdup(ba.data());
}

}

QTEST_MAIN(Point3DTest)
#include "point3dtest.moc"
