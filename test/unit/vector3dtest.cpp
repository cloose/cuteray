#include <QtTest/QtTest>

#include "vector3d.h"

class Vector3DTest : public QObject
{
    Q_OBJECT

private slots:
    void returnsLengthOfVector();
    void returnsSquaredLengthOfVector();
    void returnsNormalizedVector();
    void returnsNegatedVector();
    void calculatesProductOfScaleAndVector();
    void calculatesQuotientOfVectorAndScale();
    void calculatesSumOfTwoVectors();
    void calculatesDotProductOfTwoVectors();
};

void Vector3DTest::returnsLengthOfVector()
{
    QCOMPARE((Vector3D{2.0, -1.0, 2.0}).length(), 3.0);
    QCOMPARE((Vector3D{0.0, 0.0, 0.0}).length(), 0.0);
    QCOMPARE((Vector3D{-6.0, 8.0, 0.0}).length(), 10.0);
}

void Vector3DTest::returnsSquaredLengthOfVector()
{
    QCOMPARE((Vector3D{2.0, -1.0, 2.0}).length_squared(), 9.0);
    QCOMPARE((Vector3D{0.0, 0.0, 0.0}).length_squared(), 0.0);
    QCOMPARE((Vector3D{-6.0, 8.0, 0.0}).length_squared(), 100.0);
}

void Vector3DTest::returnsNormalizedVector()
{
    Vector3D v {2.0, -0.25, 1.5};
    QCOMPARE(v.normalized().length(), 1.0);
}

void Vector3DTest::returnsNegatedVector()
{
    Vector3D v {2.0, -0.25, 1.5};
    Vector3D expected {-2.0, 0.25, -1.5};

    QCOMPARE(-v, expected);
}

void Vector3DTest::calculatesProductOfScaleAndVector()
{
    Vector3D v {2.0, -0.25, 1.5};
    Vector3D expected {7.0, -0.875, 5.25};

    QCOMPARE(3.5 * v, expected);
}

void Vector3DTest::calculatesQuotientOfVectorAndScale()
{
    Vector3D v {2.0, -0.25, 1.5};
    Vector3D expected {0.8, -0.1, 0.6};

    QCOMPARE(v/2.5, expected);
}

void Vector3DTest::calculatesSumOfTwoVectors()
{
    Vector3D v1 {-0.5, -2.25, 10.0};
    Vector3D v2 {2.0, -0.25, 1.5};
    Vector3D expected {1.5, -2.5, 11.5};

    QCOMPARE(v1 + v2, expected);
}

void Vector3DTest::calculatesDotProductOfTwoVectors()
{
    Vector3D v1 {-0.5, -2.25, 10.0};
    Vector3D v2 {2.0, -1, 1.5};

    QCOMPARE(dot(v1, v2), 16.25);
}

namespace QTest {

template<>
char *toString(const Vector3D &vector)
{
    QByteArray ba = "Vector3D(";
    ba += QByteArray::number(vector.x) + ", " + QByteArray::number(vector.y) + ", " + QByteArray::number(vector.z);
    ba += ')';
    return qstrdup(ba.data());
}

}

QTEST_MAIN(Vector3DTest)
#include "vector3dtest.moc"
