#include <QtTest/QtTest>

#include "rgbcolor.h"

class RgbColorTest : public QObject
{
    Q_OBJECT

private slots:
    void calculatesSumOfTwoColors();
    void calculatesQuotientOfRgbColorAndNumber();
};

void RgbColorTest::calculatesSumOfTwoColors()
{
    RgbColor c1 {0.8, 0.75, 0.33};
    RgbColor c2 {1.0, 0.0, 0.15};
    RgbColor expected {1.8, 0.75, 0.48};

    QCOMPARE(c1 + c2, expected);
}

void RgbColorTest::calculatesQuotientOfRgbColorAndNumber()
{
    RgbColor c {5.0, 0.5, 0.33};
    RgbColor expected {2.0, 0.2, 0.132};

    QCOMPARE(c/2.5, expected);
}

namespace QTest {

template<>
char *toString(const RgbColor &color)
{
    QByteArray ba = "RgbColor(";
    ba += QByteArray::number(color.red()) + ", " + QByteArray::number(color.green()) + ", " + QByteArray::number(color.blue());
    ba += ')';
    return qstrdup(ba.data());
}

}

QTEST_MAIN(RgbColorTest)
#include "rgbcolortest.moc"
