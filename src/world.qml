import World 1.0
import QtQuick 2.0  // For QColor

World {
    backgroundColor: "green"

    objects: createSpheres(this)

    function createSpheres(parent) {
        var objects = []

        for (var i = 0; i < 5; i++) {
            objects.push(Qt.createQmlObject('import World 1.0; Sphere { radius: ' + Math.random() + '; color: "yellow" }', parent))
        }

        return objects
    }
}
