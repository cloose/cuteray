#include "plane.h"

#include "ray.h"
#include "shaderecord.h"

static const double kEpsilon = 0.0001;

Plane::Plane()
{

}

Point3D Plane::point() const
{
    return m_point;
}

void Plane::set_point(const Point3D &point)
{
    m_point = point;
}

Vector3D Plane::normal() const
{
    return m_normal;
}

void Plane::set_normal(const Vector3D &normal)
{
    m_normal = normal;
}

bool Plane::hit(const Ray &ray, double &tmin, ShadeRecord &record) const
{
    double t = dot((m_point - ray.origin),  m_normal) / dot(ray.direction, m_normal);

    if (t > kEpsilon) {
        tmin = t;
        record.normal = m_normal;
        record.localHitPoint = ray.point_at_parameter(t);

        return true;
    }

    return false;
}

