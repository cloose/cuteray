#ifndef MULTIOBJECTTRACER_H
#define MULTIOBJECTTRACER_H

#include "tracer.h"

class MultiObjectTracer : public Tracer
{
public:
    explicit MultiObjectTracer(World *world);

    RgbColor trace_ray(const Ray &ray) const override;
};

#endif // MULTIOBJECTTRACER_H
