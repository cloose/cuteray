#include "sampler.h"

Sampler::Sampler()
{

}

Sampler::Sampler(int numberOfSamples)
    : m_numberOfSamples(numberOfSamples)
    , m_numberOfSets(83)
    , m_count(0)
{
    m_samples.reserve(m_numberOfSamples*m_numberOfSets);
}

int Sampler::number_of_samples() const
{
    return m_numberOfSamples;
}

void Sampler::set_number_of_samples(int numberOfSamples)
{
    m_numberOfSamples = numberOfSamples;
}

int Sampler::number_of_sets() const
{
    return m_numberOfSets;
}

void Sampler::set_number_of_sets(int numberOfSets)
{
    m_numberOfSets = numberOfSets;
}

Point2D Sampler::sample_unit_square()
{
    return m_samples[m_count++ % (m_numberOfSamples*m_numberOfSets)];
}
