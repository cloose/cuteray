#ifndef CAMERA_H
#define CAMERA_H

#include "point3d.h"
#include "vector3d.h"
#include "ray.h"

struct Point2D;

class Camera
{
public:
    Camera();

    virtual Ray create_ray(const Point2D &samplePoint) const = 0;

    void set_eye(const Point3D &eye);
    void set_lookat(const Point3D &lookat);
    void set_up(const Vector3D &up);

    void compute_orthonormal_basis();

protected:
    Point3D m_eye;
    Point3D m_lookat;
    Vector3D m_up;
    Vector3D u, v, w;			// orthonormal basis vectors
};

#endif // CAMERA_H
