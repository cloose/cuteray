#ifndef VIEWPLANE_H
#define VIEWPLANE_H


class ViewPlane
{
public:
    ViewPlane();

    void set_horizontal_resolution(int horizontalResolution) { m_horizontalResolution = horizontalResolution; }
    int horizontal_resolution() const { return m_horizontalResolution; }

    void set_vertical_resolution(int verticalResolution) { m_verticalResolution = verticalResolution; }
    int vertical_resolution() const { return m_verticalResolution; }

    void set_pixel_size(float pixelSize) { m_pixelSize = pixelSize; }
    float pixel_size() const { return m_pixelSize; }

    double x(int column) const;
    double y(int row) const;

private:
    int m_horizontalResolution;
    int m_verticalResolution;
    float m_pixelSize;
};

#endif // VIEWPLANE_H
