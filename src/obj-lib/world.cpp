#include "world.h"

#include <limits>
#include "geometricobject.h"

static const double kHugeValue = std::numeric_limits<double>::max();//1.0E10;

World::World()
    : m_backgroundColor(0.0, 0.0, 0.0)
{

}

RgbColor World::backgroundColor() const
{
    return m_backgroundColor;
}

void World::setBackgroundColor(const RgbColor &backgroundColor)
{
    m_backgroundColor = backgroundColor;
}

void World::addObject(GeometricObject *object)
{
    m_objects.push_back(object);
}


std::vector<GeometricObject *> World::objects() const
{
    return m_objects;
}

void World::clearObjects()
{
    m_objects.clear();
}

ShadeRecord World::hit(const Ray &ray) const
{
    ShadeRecord rec;
    double t;
    double tmin = kHugeValue;

    for (auto object : m_objects) {
        if (object->hit(ray, t, rec) && t < tmin) {
            rec.hitAnObject = true;
            tmin = t;
            rec.color = object->color();
        }
    }

    return rec;
}
