#ifndef VECTOR3D_H
#define VECTOR3D_H

#include <cmath>
#include <array>

struct Vector3D
{
    double x, y, z;

    Vector3D() : x(0), y(0), z(0) {}
    Vector3D(double a, double b, double c) : x(a), y(b), z(c) {}

    inline double length() const {
        return sqrt(x*x + y*y + z*z);
    }

    inline double length_squared() const {
        return x*x + y*y + z*z;
    }

    inline Vector3D normalized() const {
        double l = length();
        return Vector3D(x/l, y/l, z/l);
    }

    inline Vector3D operator-() const { return Vector3D(-x, -y, -z); }
};

inline Vector3D operator*(double t, const Vector3D &v) {
    return Vector3D(t*v.x, t*v.y, t*v.z);
}

inline Vector3D operator/(const Vector3D &v, double t) {
    return Vector3D(v.x/t, v.y/t, v.z/t);
}

inline Vector3D operator+(const Vector3D &v1, const Vector3D &v2) {
    return Vector3D(v1.x + v2.x, v1.y+ v2.y, v1.z+v2.z);
}

inline Vector3D operator-(const Vector3D &v1, const Vector3D &v2) {
    return Vector3D(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
}

inline double dot(const Vector3D &v1, const Vector3D &v2) {
    return v1.x*v2.x + v1.y*v2.y + v1.z*v2.z;
}

inline Vector3D cross(const Vector3D &v1, const Vector3D &v2) {
    return Vector3D(v1.y*v2.z - v1.z*v2.y, v1.z*v2.x - v1.x*v2.z, v1.x*v2.y - v1.y*v2.x);
}

inline bool operator==(const Vector3D &lhs, const Vector3D &rhs) {
    return lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z;
}

#endif // VECTOR3D_H
