#include "viewplane.h"

ViewPlane::ViewPlane()
    : m_horizontalResolution(200)
    , m_verticalResolution(200)
    , m_pixelSize(1.0)
{

}

double ViewPlane::x(int column) const
{
    return m_pixelSize * (column - m_horizontalResolution / 2.0 + 0.5);
}

double ViewPlane::y(int row) const
{
    return m_pixelSize * (row - m_verticalResolution / 2.0 + 0.5);
}
