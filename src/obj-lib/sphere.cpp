#include "sphere.h"

#include "ray.h"
#include "vector3d.h"
#include "shaderecord.h"

static const double kEpsilon = 0.0001;

Sphere::Sphere()
{

}

Point3D Sphere::center() const
{
    return m_center;
}

void Sphere::set_center(const Point3D &center)
{
    m_center = center;
}

double Sphere::radius() const
{
    return m_radius;
}

void Sphere::set_radius(double radius)
{
    m_radius = radius;
}

bool Sphere::hit(const Ray &ray, double &tmin, ShadeRecord &record) const
{
    Vector3D oc = ray.origin - m_center;

    double a = dot(ray.direction, ray.direction);
    double b = 2.0 * dot(oc, ray.direction);
    double c = dot(oc, oc) - m_radius*m_radius;

    double discriminant = b*b - 4.0*a*c;
    if (discriminant > 0) {
        double result = (-b - sqrt(discriminant)) / (2.0*a);

        if (result > kEpsilon) {
            tmin = result;
            record.localHitPoint = ray.point_at_parameter(result);
            record.normal = (record.localHitPoint - m_center) / m_radius;
            return true;
        }

        result = (-b + sqrt(discriminant)) / (2.0*a);

        if (result > kEpsilon) {
            tmin = result;
            record.localHitPoint = ray.point_at_parameter(result);
            record.normal = (record.localHitPoint - m_center) / m_radius;
            return true;
        }
    }

    return false;
}

