#include "randomnumbers.h"

#include <random>

static std::mt19937_64 generator;
static std::uniform_real_distribution<double> distribution(0, 1);

double random_number()
{
    return distribution(generator);
}
