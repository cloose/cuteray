#ifndef PLANE_H
#define PLANE_H

#include "geometricobject.h"

#include "point3d.h"
#include "vector3d.h"

class Plane : public GeometricObject
{
public:
    Plane();

    Point3D point() const;
    void set_point(const Point3D &point);

    Vector3D normal() const;
    void set_normal(const Vector3D &normal);

    bool hit(const Ray &ray, double &tmin, ShadeRecord &record) const override;

private:
    Point3D m_point;
    Vector3D m_normal;
};

#endif // PLANE_H
