#ifndef SHADERECORD_H
#define SHADERECORD_H

#include "rgbcolor.h"
#include "point3d.h"
#include "vector3d.h"

struct ShadeRecord
{
    bool hitAnObject;
    Point3D localHitPoint;	// world coordinates of hit point
    Vector3D normal;				// normal at hit point
    RgbColor color;

    ShadeRecord() : hitAnObject(false), localHitPoint{0,0,0}, normal{0,0,0}, color{0,0,0} {}
};

#endif // SHADERECORD_H
