#ifndef JITTEREDSAMPLER_H
#define JITTEREDSAMPLER_H

#include "sampler.h"

class JitteredSampler : public Sampler
{
public:
    JitteredSampler();
    explicit JitteredSampler(int numberOfSamples);

    void generate_samples() override;
};

#endif // JITTEREDSAMPLER_H
