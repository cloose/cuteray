#include "multiobjecttracer.h"

#include "shaderecord.h"
#include "world.h"

MultiObjectTracer::MultiObjectTracer(World *world)
    : Tracer(world)
{

}

RgbColor MultiObjectTracer::trace_ray(const Ray &ray) const
{
    ShadeRecord sr = m_world->hit(ray);
    if (sr.hitAnObject) {
        return sr.color;
    } else {
        return m_world->backgroundColor();
    }
}
