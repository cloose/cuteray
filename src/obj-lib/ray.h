#ifndef RAY_H
#define RAY_H

#include "point3d.h"
#include "vector3d.h"

struct Ray
{
    Point3D origin;
    Vector3D direction;

    Point3D point_at_parameter(double t) const { return origin + t*direction; }
};


#endif // RAY_H
