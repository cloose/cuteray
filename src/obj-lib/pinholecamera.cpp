#include "pinholecamera.h"

#include "point2d.h"

PinholeCamera::PinholeCamera()
{

}

void PinholeCamera::set_view_plane_distance(double distance)
{
    m_viewPlaneDistance = distance;
}

Ray PinholeCamera::create_ray(const Point2D &samplePoint) const
{
    Ray ray;
    ray.origin = m_eye;
    ray.direction = (samplePoint.x*u + samplePoint.y*v - m_viewPlaneDistance*w).normalized();
    return ray;
}

