#ifndef GEOMETRICOBJECT_H
#define GEOMETRICOBJECT_H

#include "rgbcolor.h"

struct Ray;
struct ShadeRecord;

class GeometricObject {
public:
    virtual ~GeometricObject() {}

    RgbColor color() const;
    void set_color(const RgbColor &color);

    virtual bool hit(const Ray &ray, double &tmin, ShadeRecord &record) const = 0;

private:
    RgbColor m_color;
};

#endif // GEOMETRICOBJECT_H

