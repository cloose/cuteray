#ifndef SPHERE_H
#define SPHERE_H

#include "geometricobject.h"

#include "point3d.h"

class Sphere : public GeometricObject
{
public:
    Sphere();

    Point3D center() const;
    void set_center(const Point3D &center);

    double radius() const;
    void set_radius(double radius);

    bool hit(const Ray &ray, double &tmin, ShadeRecord &record) const override;

private:
    Point3D m_center;
    double m_radius;
};

#endif // SPHERE_H
