#ifndef PINHOLECAMERA_H
#define PINHOLECAMERA_H

#include "camera.h"

class PinholeCamera : public Camera
{
public:
    PinholeCamera();

    void set_view_plane_distance(double distance);

    Ray create_ray(const Point2D &samplePoint) const override;

private:
    double m_viewPlaneDistance;
};

#endif // PINHOLECAMERA_H
