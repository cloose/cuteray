#include "geometricobject.h"

RgbColor GeometricObject::color() const
{
    return m_color;
}

void GeometricObject::set_color(const RgbColor &color)
{
    m_color = color;
}
