#ifndef POINT2D_H
#define POINT2D_H

struct Point2D
{
    double x, y;

    Point2D() : x(0), y(0) {}
    Point2D(double a, double b) : x(a), y(b) {}
};

#endif // POINT2D_H
