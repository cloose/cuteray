#ifndef SAMPLER_H
#define SAMPLER_H

#include <vector>
#include "point2d.h"

class Sampler
{
public:
    Sampler();
    explicit Sampler(int numberOfSamples);

    int number_of_samples() const;
    void set_number_of_samples(int numberOfSamples);

    int number_of_sets() const;
    void set_number_of_sets(int numberOfSets);

    virtual void generate_samples() = 0;

    Point2D sample_unit_square();

protected:
    int m_numberOfSamples;
    int m_numberOfSets;
    unsigned long m_count;
    std::vector<Point2D> m_samples;
};

#endif // SAMPLER_H
