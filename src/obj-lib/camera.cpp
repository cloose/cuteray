#include "camera.h"

Camera::Camera()
{

}

void Camera::set_eye(const Point3D &eye) {
    m_eye = eye;
}

void Camera::set_lookat(const Point3D &lookat) {
    m_lookat = lookat;
}

void Camera::set_up(const Vector3D &up) {
    m_up = up;
}

void Camera::compute_orthonormal_basis()
{
    w = (m_eye - m_lookat).normalized();
    u = cross(m_up, w).normalized();
    v = cross(w, u);
}

