#ifndef RGBCOLOR_H
#define RGBCOLOR_H

class RgbColor {
public:
    RgbColor() : m_red(0.0), m_green(0.0), m_blue(0.0) {}
    RgbColor(double r, double g, double b) : m_red(r), m_green(g), m_blue(b) {}

    double red() const { return m_red; }
    double green() const { return m_green; }
    double blue() const { return m_blue; }

private:
    double m_red, m_green, m_blue;
};

inline RgbColor operator+(const RgbColor &c1, const RgbColor &c2) {
    return RgbColor(c1.red() + c2.red(), c1.green() + c2.green(), c1.blue() + c2.blue());
}

inline RgbColor operator/(const RgbColor &c, double t) {
    return RgbColor(c.red()/t, c.green()/t, c.blue()/t);
}

inline bool operator==(const RgbColor &lhs, const RgbColor &rhs) {
    return lhs.red() == rhs.red() && lhs.green() == rhs.green() && lhs.blue() == rhs.blue();
}

#endif // RGBCOLOR_H

