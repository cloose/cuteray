#include "jitteredsampler.h"

#include <cmath>

#include "point2d.h"
#include "randomnumbers.h"

JitteredSampler::JitteredSampler()
    : JitteredSampler(1)
{
}

JitteredSampler::JitteredSampler(int numberOfSamples)
    : Sampler(numberOfSamples)
{
    generate_samples();
}

void JitteredSampler::generate_samples()
{
    int n = (int)sqrt(m_numberOfSamples);

    for (int s = 0; s < m_numberOfSets; ++s) {
        for (int j = 0; j < n; ++j) {
            for (int i = 0; i < n; ++i) {
                double x = (i + random_number()) / n;
                double y = (j + random_number()) / n;
                Point2D samplePoint(x, y);
                m_samples.push_back(samplePoint);
            }
        }
    }
}
