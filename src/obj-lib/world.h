#ifndef WORLD_H
#define WORLD_H

#include <vector>
#include "rgbcolor.h"
#include "shaderecord.h"

class GeometricObject;
struct Ray;

class World
{
public:
    World();

    RgbColor backgroundColor() const;
    void setBackgroundColor(const RgbColor &backgroundColor);

    void addObject(GeometricObject *object);

    std::vector<GeometricObject*> objects() const;
    void clearObjects();

    ShadeRecord hit(const Ray &ray) const;

private:
    RgbColor m_backgroundColor;
    std::vector<GeometricObject*> m_objects;
};

#endif // WORLD_H
