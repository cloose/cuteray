#ifndef POINT3D_H
#define POINT3D_H

#include "vector3d.h"

struct Point3D
{
    double x, y, z;

    Point3D() : x(0), y(0), z(0) {}
    Point3D(double a, double b, double c) : x(a), y(b), z(c) {}
};


inline Point3D operator+(const Point3D &p, const Vector3D &v) {
    return Point3D(p.x + v.x, p.y + v.y, p.z + v.z);
}

inline Vector3D operator-(const Point3D &p1, const Point3D &p2) {
    return Vector3D(p1.x - p2.x, p1.y - p2.y, p1.z - p2.z);
}

inline bool operator==(const Point3D &lhs, const Point3D &rhs) {
    return lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z;
}

#endif // POINT3D_H
