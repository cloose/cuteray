#ifndef TRACER_H
#define TRACER_H

struct Ray;
struct RgbColor;
class World;

class Tracer
{
public:
    explicit Tracer(World *world);

    virtual RgbColor trace_ray(const Ray &ray) const = 0;

protected:
    World *m_world;
};

#endif // TRACER_H
