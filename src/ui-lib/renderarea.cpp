#include "renderarea.h"

#include <QPainter>
#include <QPaintEvent>


RenderArea::RenderArea(QWidget *parent)
    : QWidget(parent)
{

}

void RenderArea::setRenderedScene(const QImage &image)
{
    scene = image;
    update();
}

void RenderArea::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    QRect dirtyRect = event->rect();
    painter.drawImage(dirtyRect, scene, dirtyRect);
}
