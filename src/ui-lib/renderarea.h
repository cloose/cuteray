#ifndef RENDERAREA_H
#define RENDERAREA_H

#include <QWidget>

class RenderArea : public QWidget
{
    Q_OBJECT
public:
    explicit RenderArea(QWidget *parent = 0);

    void setRenderedScene(const QImage &image);

protected:
    void paintEvent(QPaintEvent *event) override;

private:
    QImage scene;
};

#endif // RENDERAREA_H
