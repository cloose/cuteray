#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <chrono>

#include <QDebug>
#include <QQmlEngine>
#include <QQmlComponent>

#include "qmlsphere.h"
#include "qmlworld.h"
#include "point3d.h"
#include "vector3d.h"
#include "ray.h"
#include "world.h"
#include "plane.h"
#include "sphere.h"
#include "geometricobject.h"
#include "multiobjecttracer.h"
#include "viewplane.h"
#include "typeconversions.h"
#include "jitteredsampler.h"

using namespace std;

QDebug operator<<(QDebug debug, const RgbColor &color)
{
    QDebugStateSaver saver(debug);
    debug.nospace() << "RgbColor[r=" << color.red() << ",g=" << color.green() << ",b=" << color.blue() << "]";
    return debug;
}

QDebug operator<<(QDebug debug, const Point3D &point)
{
    QDebugStateSaver saver(debug);
    debug.nospace() << "Point3D[x=" << point.x << ",y=" << point.y << ",z=" << point.z << "]";
    return debug;
}

QDebug operator<<(QDebug debug, const Vector3D &vector)
{
    QDebugStateSaver saver(debug);
    debug.nospace() << "Vector3D[x=" << vector.x << ",y=" << vector.y << ",z=" << vector.z << "]";
    return debug;
}

QDebug operator<<(QDebug debug, GeometricObject *object)
{
    QDebugStateSaver saver(debug);
    debug.nospace() << '(' << object->color() << ')';
    return debug;
}

QDebug operator<<(QDebug debug, Sphere *sphere)
{
    QDebugStateSaver saver(debug);
    debug.nospace() << "Sphere [center=" << sphere->center() << ",radius='" << sphere->radius() << "',color=" << sphere->color() << "]";
    return debug;
}

QDebug operator<<(QDebug debug, Plane *plane)
{
    QDebugStateSaver saver(debug);
    debug.nospace() << "Plane [point=" << plane->point() << ",normal=" << plane->normal() << ",color=" << plane->color() << "]";
    return debug;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::renderSceneClicked()
{
    QQmlEngine engine;
    QQmlComponent component(&engine, "example1.qml");
    QmlWorld *world = qobject_cast<QmlWorld*>(component.create());
    if (world) {
        qWarning() << "The world's background color is" << world->backgroundColor();
        qWarning() << "The world's background color is" << world->world()->backgroundColor();
        qWarning() << "The world has" << world->world()->objects().size() << "objects";
        for (auto o : world->world()->objects()) {
            if (Sphere *s = dynamic_cast<Sphere*>(o)) {
                qWarning() << "The world contains" << s;
            }
            if (Plane *p = dynamic_cast<Plane*>(o)) {
                qWarning() << "The world contains" << p;
            }
        }
    } else {
        qWarning() << component.errors();
    }

    double zw = 100.0;

    ViewPlane *viewPlane = new ViewPlane();
    viewPlane->set_horizontal_resolution(200);
    viewPlane->set_vertical_resolution(200);
    viewPlane->set_pixel_size(1.0);

    QImage scene { viewPlane->horizontal_resolution(), viewPlane->vertical_resolution(), QImage::Format_ARGB32 };

    Tracer *tracer = new MultiObjectTracer(world->world());
    Sampler *sampler = new JitteredSampler(25);

    qDebug() << chrono::high_resolution_clock::period::den;
    auto start_time = chrono::high_resolution_clock::now();

    for (int r = 0; r < viewPlane->vertical_resolution(); ++r) {
        for (int c = 0; c < viewPlane->horizontal_resolution(); ++c) {
            RgbColor pixelColor {0.0, 0.0, 0.0};

            for (int s = 0; s < 25; ++s) {
                Point2D sp = sampler->sample_unit_square();
                double x = viewPlane->pixel_size() * (c - 0.5*viewPlane->horizontal_resolution() + sp.x);
                double y = viewPlane->pixel_size() * (r - 0.5*viewPlane->vertical_resolution() + sp.y);

                Ray ray;
                ray.direction = Vector3D{0, 0, -1};
                ray.origin = Point3D(x, y, zw);
                pixelColor = pixelColor + tracer->trace_ray(ray);
            }

            pixelColor = pixelColor / 25;
            int x = c;
            int y = viewPlane->vertical_resolution() - r - 1;
            //scene.setPixelColor(x, y, toQColor(pixelColor));
            scene.setPixel(x, y, toQColor(pixelColor).rgba());
        }
    }
    auto end_time = chrono::high_resolution_clock::now();

    qDebug() << chrono::duration_cast<chrono::duration<double>>(end_time - start_time).count() << "s";

    ui->widget->setRenderedScene(scene);
}
