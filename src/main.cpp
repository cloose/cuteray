#include <QApplication>

#include "mainwindow.h"

#include <QQmlComponent>

#include "qmlgeometricobject.h"
#include "qmlplane.h"
#include "qmlsphere.h"
#include "qmlworld.h"

void registerQmlTypes() {
    qmlRegisterType<QmlWorld>("World", 1,0, "World");
    qmlRegisterType<QmlGeometricObject>();
    qmlRegisterType<QmlPlane>("World", 1,0, "Plane");
    qmlRegisterType<QmlSphere>("World", 1,0, "Sphere");
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setApplicationDisplayName("CuteRay");

    registerQmlTypes();

    MainWindow win;
    win.show();

    return app.exec();
}
