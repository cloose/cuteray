import World 1.0
import QtQuick 2.0  // For QColor

World {
    backgroundColor: "black"

    objects: [
        Sphere { center: Qt.vector3d(0.0, -25.0, 0.0)
                 radius: 80.0
                 color: "red"
        },
        Sphere { center: Qt.vector3d(0.0, 30.0, 0.0)
                 radius: 60.0
                 color: "yellow"
        },
        Plane { point: Qt.vector3d(0, 0, 0)
                normal: Qt.vector3d(0, 1, 1)
                color: Qt.rgba(0.0,0.3,0.0,1.0)
        }
    ]

}
