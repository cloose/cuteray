#ifndef TYPECONVERSIONS_H
#define TYPECONVERSIONS_H

#include <QColor>
#include <QVector3D>

#include "point3d.h"
#include "rgbcolor.h"

RgbColor toRgbColor(const QColor &color);

QColor toQColor(const RgbColor &color);

Point3D toPoint3D(const QVector3D &point);

QVector3D toQVector3D(const Point3D &point);

Vector3D toVector3D(const QVector3D &vector);

QVector3D toQVector3D(const Vector3D &vector);

#endif // TYPECONVERSIONS_H

