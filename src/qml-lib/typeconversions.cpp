#include "typeconversions.h"

RgbColor toRgbColor(const QColor &color) {
    return RgbColor(color.redF(), color.greenF(), color.blueF());
}

QColor toQColor(const RgbColor &color) {
    return QColor::fromRgbF(color.red(), color.green(), color.blue());
}

Point3D toPoint3D(const QVector3D &point)
{
    return Point3D(point.x(), point.y(), point.z());
}

QVector3D toQVector3D(const Point3D &point)
{
    return QVector3D(point.x, point.y, point.z);
}

Vector3D toVector3D(const QVector3D &vector)
{
    return Vector3D(vector.x(), vector.y(), vector.z());
}

QVector3D toQVector3D(const Vector3D &vector)
{
    return QVector3D(vector.x, vector.y, vector.z);
}
