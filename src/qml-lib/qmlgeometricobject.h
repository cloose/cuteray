#ifndef QMLGEOMETRICOBJECT_H
#define QMLGEOMETRICOBJECT_H

#include <QObject>
#include <QColor>

class GeometricObject;

class QmlGeometricObject : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QColor color READ color WRITE setColor)
public:
    explicit QmlGeometricObject(QObject *parent = 0);

    QColor color() const;
    void setColor(const QColor &color);

    virtual GeometricObject *object() const = 0;
};

#endif // QMLGEOMETRICOBJECT_H
