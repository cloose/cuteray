#include "qmlgeometricobject.h"

#include "geometricobject.h"
#include "typeconversions.h"

QmlGeometricObject::QmlGeometricObject(QObject *parent)
    : QObject(parent)
{

}

QColor QmlGeometricObject::color() const
{
    return toQColor(object()->color());
}

void QmlGeometricObject::setColor(const QColor &color)
{
    object()->set_color(toRgbColor(color));
}
