#ifndef QMLPLANE_H
#define QMLPLANE_H

#include "qmlgeometricobject.h"

#include <QVector3D>

class Plane;

class QmlPlane : public QmlGeometricObject
{
    Q_OBJECT
    Q_PROPERTY(QVector3D point READ point WRITE setPoint)
    Q_PROPERTY(QVector3D normal READ normal WRITE setNormal)
public:
    explicit QmlPlane(QObject *parent = 0);

    QVector3D point() const;
    void setPoint(const QVector3D &point);

    QVector3D normal() const;
    void setNormal(const QVector3D &normal);

    GeometricObject *object() const override;

private:
    Plane *m_plane;
};

#endif // QMLPLANE_H
