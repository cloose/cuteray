#include "qmlworld.h"

#include "typeconversions.h"
#include "world.h"

#include "qmlgeometricobject.h"

QmlWorld::QmlWorld(QObject *parent)
    : QObject(parent)
    , m_world(new World())
{
}

QColor QmlWorld::backgroundColor() const
{
    return toQColor(m_world->backgroundColor());
}

void QmlWorld::setBackgroundColor(const QColor &backgroundColor)
{
    m_world->setBackgroundColor(toRgbColor(backgroundColor));
}

QQmlListProperty<QmlGeometricObject> QmlWorld::objects()
{
    auto append = [](QQmlListProperty<QmlGeometricObject> *list, QmlGeometricObject *object) { reinterpret_cast<QmlWorld*>(list->data)->addObject(object); };
    auto count = [](QQmlListProperty<QmlGeometricObject> *list) { return reinterpret_cast<QmlWorld*>(list->data)->objectCount(); };
    auto at = [](QQmlListProperty<QmlGeometricObject> *list, int idx) { return reinterpret_cast<QmlWorld*>(list->data)->objectAt(idx); };
    auto clear = [](QQmlListProperty<QmlGeometricObject> *list) { reinterpret_cast<QmlWorld*>(list->data)->clearObjects(); };

    return QQmlListProperty<QmlGeometricObject>(this, this, append, count, at, clear);
}

int QmlWorld::objectCount() const
{
    return m_objects.count();
}

void QmlWorld::clearObjects()
{
    m_world->clearObjects();
    m_objects.clear();
}

QmlGeometricObject *QmlWorld::objectAt(int index) const
{
    return m_objects.at(index);
}

void QmlWorld::addObject(QmlGeometricObject *object)
{
    m_world->addObject(object->object());
    m_objects.append(object);
}

World *QmlWorld::world() const
{
    return m_world;
}

