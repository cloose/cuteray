#ifndef QMLWORLD_H
#define QMLWORLD_H

#include <QObject>
#include <QQmlListProperty>
#include <QColor>

class QmlGeometricObject;
class World;

class QmlWorld : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QColor backgroundColor READ backgroundColor WRITE setBackgroundColor)
    Q_PROPERTY(QQmlListProperty<QmlGeometricObject> objects READ objects)
public:
    explicit QmlWorld(QObject *parent = 0);

    QColor backgroundColor() const;
    void setBackgroundColor(const QColor &backgroundColor);

    QQmlListProperty<QmlGeometricObject> objects();
    int objectCount() const;
    void clearObjects();

    QmlGeometricObject* objectAt(int index) const;
    void addObject(QmlGeometricObject *object);

    World *world() const;

private:
    World *m_world;
    QList<QmlGeometricObject*> m_objects;
};

#endif // QMLWORLD_H
