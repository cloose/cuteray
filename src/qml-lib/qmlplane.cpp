#include "qmlplane.h"

#include "plane.h"
#include "typeconversions.h"

QmlPlane::QmlPlane(QObject *parent)
    : QmlGeometricObject(parent)
    , m_plane(new Plane())
{

}

QVector3D QmlPlane::point() const
{
    return toQVector3D(m_plane->point());
}

void QmlPlane::setPoint(const QVector3D &point)
{
    m_plane->set_point(toPoint3D(point));
}

QVector3D QmlPlane::normal() const
{
    return toQVector3D(m_plane->normal());
}

void QmlPlane::setNormal(const QVector3D &normal)
{
    m_plane->set_normal(toVector3D(normal));
}

GeometricObject *QmlPlane::object() const
{
    return m_plane;
}

