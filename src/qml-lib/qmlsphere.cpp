#include "qmlsphere.h"

#include "sphere.h"
#include "typeconversions.h"

QmlSphere::QmlSphere(QObject *parent)
    : QmlGeometricObject(parent)
    , m_sphere(new Sphere())
{
}

QVector3D QmlSphere::center() const
{
    return toQVector3D(m_sphere->center());
}

void QmlSphere::setCenter(const QVector3D &center)
{
    m_sphere->set_center(toPoint3D(center));
}

double QmlSphere::radius() const
{
    return m_sphere->radius();
}

void QmlSphere::setRadius(double radius)
{
    m_sphere->set_radius(radius);
}

GeometricObject *QmlSphere::object() const
{
    return m_sphere;
}
