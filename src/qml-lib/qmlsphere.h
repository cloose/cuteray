#ifndef QMLSPHERE_H
#define QMLSPHERE_H

#include "qmlgeometricobject.h"

#include <QVector3D>

class Sphere;

class QmlSphere : public QmlGeometricObject
{
    Q_OBJECT
    Q_PROPERTY(double radius READ radius WRITE setRadius)
    Q_PROPERTY(QVector3D center READ center WRITE setCenter)
public:
    explicit QmlSphere(QObject *parent = 0);

    QVector3D center() const;
    void setCenter(const QVector3D &center);

    double radius() const;
    void setRadius(double radius);

    GeometricObject *object() const override;

private:
    Sphere *m_sphere;
};

#endif // QMLSPHERE_H
